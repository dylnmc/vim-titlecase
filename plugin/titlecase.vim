
" TODO
"  - Don't titlecase articles ("a, an, the") optionally
"  - Titlecase hyphenated words
"  - Allow filetype blacklists (do we need whitelists?)

let s:word_match = '\<\(\k\)\(\k*''*\k*\)\>'
let s:toUpper = '\u\1\L\2'

function! s:titlecase(type, ...)
	echom '<'.string(a:type).'>'
	if a:0
		if a:type =~ '\v'
			let l:save = @"
			silent execute 'normal! `<'.a:type.'`>y'
			call setreg('@', substitute(@@, s:word_match, s:toUpper, 'g'), 'b')
			silent execute 'normal! '.a:type.'`>p'
			let @" = l:save
		else
			silent execute 'normal! `<'.a:type . '`>y'
			silent execute 'normal! '.a:type.'`>"ip'
		endif
	elseif a:type ==# 'line'
		execute '''[,'']s/'.s:word_match.'/'.s:toUpper.'/ge'
	else
		let l:save = @"
		silent execute 'normal! `[v`]y'
		silent execute 'normal! v`]c'.substitute(@@, s:word_match, s:toUpper, 'g')
		let @" = l:save
	endif
endfunction

xnoremap <silent> <plug>(titlecase) :<c-u>call <sid>titlecase(visualmode(),visualmode() ==# 'V' ? 1 : 0)<cr>
nnoremap <silent> <plug>(titlecase) :<c-u>set opfunc=<sid>titlecase<cr>g@
nnoremap <silent> <expr> <plug>(titlecaseLine) execute('set opfunc=<sid>titlecase').v:count1.'g@_'

if get(g:, 'titlecase_default_maps', 1)
	nmap <leader>gt <plug>(titlecase)
	nmap <leader>gtt <plug>(titlecaseLine)
	xmap <leader>gt <plug>(titlecase)
endif

